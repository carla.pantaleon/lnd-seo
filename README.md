## Goal

Your goal is to apply the techniques you learned from the video sessions about Search Engine Optimization and Frontend Optimization, to create an optimized version of this web application.

![Banner Image](lighthouse-results.png)

> Google DevTools Lighthouse Metrics before optimization

You need to increase the following metrics shown above by implementing proper Search Engine Optimization and Frontend Optimization.

### Getting Started

1. Clone Repository

```
git clone https://gitlab.com/carla.pantaleon/lnd-seo
```

2. Run the development server:

```bash
npm run dev
# or
yarn dev
```

### Result

> You can access it here: https://lnd-seo.vercel.app/

![Banner Image](seo-result.png)