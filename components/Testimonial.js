const Testimonial = () => {
    return(
        <aside className="text-center testimonial-banner">
        <div className="container px-5">
          <div className="row gx-5 justify-content-center">
            <div className="col-xl-8">
              <h2 className="h2 fs-1 text-white mb-4">"An intuitive solution to a common problem that we all face, wrapped up in a single app!"</h2>
              <img src="assets/img/tnw-logo.svg" height={'48px'} width={'114px'} style={{height: '3rem'}} alt="The Next Web"/>
            </div>
          </div>
        </div>
      </aside>
    )
}

export default Testimonial